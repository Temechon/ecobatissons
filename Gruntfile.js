module.exports = function(grunt) {

    require('time-grunt')(grunt);

    // load all grunt tasks
    require('jit-grunt')(grunt);

    grunt.initConfig({

        jade: {
            html: {
                files: {
                    '.': ['jade/*.jade',  "!jade/contact.jade"]
                },
                options: {
                    client: false
                }
            }
        },

        jade4php: {
            compile: {
                src: ['jade/contact.jade'],
                dest: 'contact.php',
                ext: '.php'
            }
        },
        // Watches content related changes
        watch : {
            sass : {
                files: ['sass/**/*.scss'],
                tasks: ['sass','postcss:debug']
            },
            jade : {
                files: ['jade/**/*.jade', 'includes/**/*.jade'],
                tasks: ['jade', 'jade4php']
            }
        },
        // Sass compilation. Produce an extended css file in css folder
        sass : {
            options: {
                style: 'expanded'
            },
            dist : {
                files: {
                    'css/main.css': 'sass/main.scss'
                }
            }
        },
        // Auto prefixer css
        postcss : {
            debug : {
                options: {
                    processors: [
                        require('autoprefixer')({browsers: 'last 2 versions'})
                    ]
                },
                src: 'css/main.css'
            },
            dist: {
                options: {
                    processors: [
                        require('autoprefixer')({browsers: 'last 2 versions'}),
                        require('cssnano')()
                    ]
                },
                src: 'css/main.css'
            }
        },
        //Server creation
        connect: {
            server: {
                options: {
                    port: 3000,
                    base: '.'
                }
            },
            test: {
                options: {
                    port: 3000,
                    base: '.',
                    keepalive:true
                }
            }
        },
        // Open default browser
        open: {
            local: {
                path: 'http://localhost:3000/'
            }
        }
    });

    grunt.registerTask('default', 'Compile and watch source files', [
        'dev',
        'connect:server',
        'open',
        'watch'
    ]);

    grunt.registerTask('dev', 'build dev version', [
        'jade',
        'jade4php',
        'sass',
        'postcss:debug'
    ]);
};