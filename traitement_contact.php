<?php 
// Launch the session and initialize session variables
session_start();


$privateKey = "6LdSrAkUAAAAAN0NZDprbgEzX4o8mi1hE29_kn0i"; // recaptcha v2

// Initialize the variable errorMessage
$_SESSION['SendMailMessage'] = "";

// Securise all the fields and stock all the datas
$name = htmlspecialchars($_POST['nom']);
$firstName = htmlspecialchars($_POST['prenom']);
$phone = htmlspecialchars($_POST['phone']);
$mail = htmlspecialchars($_POST['mail']);
$message = htmlspecialchars($_POST['message']);
$ville = htmlspecialchars($_POST['ville']);

// check if captcha is validated by Google API
if(isset($_POST['g-recaptcha-response'])){
    $captcha = $_POST['g-recaptcha-response'];
    $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$privateKey."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']));
}
var_dump($response);
if(!$response->success){
    // Send captcha error message

    $_SESSION['SendMailMessage'] = "Erreur : captcha manquant";
    header('Location: contact.php');
} else {

    // Choose the adress mail
    $mailToSend = 'julian.chenard@gmail.com';
    // Define the content of the message
    $message_txt = $message." <br/><br/>Envoyé par ". $name." ".$firstName."<br/>Téléphone: ".$phone."<br/>Email: ".$mail."<br/>Ville : ".$ville;

    // Creation of the boundary
    $boundary = "-----=".md5(rand());

    // The jump line variable
    $passage_ligne = "\n";

    // The header content
    $headers  = "MIME-Version: 1.0 \n";
    $headers .= "Content-Transfer-Encoding: 8bit \n";
    $headers .= "Content-type: text/html; charset=utf-8 \n";
    $headers .= "From: siteweb@ecobatissons.com  \n";


    // Define the subject of the email
    $sujet = "Message envoyé via le formulaire du contact de ecobatissons.com";

    $contenu = "";
    $contenu .= $sujet."<br/>";
    $contenu .= $message_txt."<br/>";

    // Create the message


    // Send the email
    $ret = @mail($mailToSend, $sujet, $contenu, $headers);
    $_SESSION['SendMailMessage'] = "Votre message a bien été envoyé ! Merci !";
    header('Location: contact.php');
}